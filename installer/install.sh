#!/bin/bash

HELM="/home/ec2-user/.local/bin/helm"

$HELM repo update

chrts=$(cat names.txt | cut -d " " -f1)

for chart in $chrts;
do
	cp ../charts/$chart/values.yaml yamls/$chart.yaml
	chartname=$(cat names.txt | grep -i $chart | cut -d " " -f2)
	$HELM list | grep -i $chart
	if [ $(echo $?) = 0 ]
	then
		$HELM upgrade -f yamls/$chart.yaml $chart $chartname
	else
		$HELM install $chart $chartname
	fi
done
