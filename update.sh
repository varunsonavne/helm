#!/bin/bash

imgname=$(cat charts/$1/values.yaml | grep -i repository | cut -d " " -f4)

latimgname="harbor.sa-dev.info/stl/storefront/dev-v2:$2"

sed -i "s|$imgname|$latimgname|g" charts/$1/values.yaml

git add .

git commit -m "adding $latimgname in $1 values.yaml"

git push origin dev-v2

#/home/ec2-user/.local/bin/helm package charts/$1
